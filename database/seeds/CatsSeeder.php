<?php

use Illuminate\Database\Seeder;
use App\Model\Cat;

class CatsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Cat::truncate();

      App\Model\Cat::create([
          'name' => 'Tom',
          'age' => 2,
          'weight' => 3,
      ]);

      DB::table('cats')->insert([
            'name' => str_random(10),
            'age' => 2,
            'weight' => 8,
            'created_at' => now(),
            'updated_at' => now()
      ]);

      factory(App\Model\Cat::class, 5)->create();
    }
}
