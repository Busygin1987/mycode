<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Cat::class, function (Faker $faker) {
    return [
      'name' => $faker->name,
      'age' => $faker->randomDigit,
      'weight' => $faker->randomDigit
    ];
});
