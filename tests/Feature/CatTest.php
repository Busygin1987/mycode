<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Model\Cat;

use DatabaseTransactions;
//use DatabaseMigrations;

class CatTest extends TestCase
{
  public function setUp()
    {
        parent::setUp();

        Cat::truncate();
        // $this->cat = factory(Cat::class)->create([
        //     'name' => 'Tom',
        // ]);
        // $this->cat2 = factory(Cat::class)->create();
    }


  public function testsCatsAreCreatedCorrectly ()
  {
    $payload = [
            'name' => 'Tom',
            'age' => 2,
            'weight' => 3
        ];

    $this->json('POST', '/api/cats', $payload)
         ->assertStatus(201)
         ->assertJsonFragment(['name' => 'Tom', 'age' => 2, 'weight' => 3]);

    $this->assertDatabaseHas('cats', [
            'name' => 'Tom',
            'age' => 2,
            'weight' => 3,
            'amount_of_legs' => 4
             ]);
  }


  public function testsCatsAreUpdatedCorrectly ()
  {
    $cat = factory(Cat::class)->create();

    $payload = [
              'name' => 'BugCat',
              'age' => 7,
              'weight' => 14
    ];

    $response = $this->json('PUT', '/api/cats/' . $cat->id, $payload)
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'BugCat',
                'age' => 7,
                'weight' => 14
            ]);
			
	$this->assertDatabaseHas('cats', [
				'name' => 'BugCat',
                'age' => 7,
                'weight' => 14
            ]);			
  }


  public function testsCatsAreDeletedCorrectly ()
  {
    $cat = factory(Cat::class)->create();

    $this->json('DELETE', '/api/cats/' . $cat->id)
            ->assertStatus(410);
  }


  public function testCatsAreListedCorrectly()
  {
    
    $cat = factory(Cat::class)->create([
            'name' => 'Error',
            'age' => 9,
            'weight' => 18
    ]);
    $cat = factory(Cat::class)->create([
            'name' => 'Bug',
            'age' => 7,
            'weight' => 14
    ]);

    $response = $this->json('GET', '/api/cats')
           ->assertStatus(200)
           ->assertJson([
             
                    [
                      'name' => 'Error',
                      'age' => 9,
                      'weight' => 18,
                      'amount_of_legs' => 4
                    ],
                    [
                      'name' => 'Bug',
                      'age' => 7,
                      'weight' => 14,
                      'amount_of_legs' => 4
                  ]
              
           ]);
  }
  
  
  public function testCatAreListedCorrectly ()
  {
	  $cat = factory(Cat::class)->create([
            'name' => 'Laravel',
            'age' => 3,
            'weight' => 15
	  ]);
	  
	  $response = $this->json('GET', '/api/cats/' . $cat->id)
           ->assertStatus(200)
           ->assertJson([     
                      'name' => 'Laravel',
                      'age' => 3,
                      'weight' => 15,
                      'amount_of_legs' => 4
              
           ]);

  }
  
  public function testCatNotFound ()
  {
	  $response = $this->get('/')
           ->assertStatus(404);
  }
}
