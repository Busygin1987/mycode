<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CatResource extends JsonResource
{
  
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);

        return [
           'id' => $this->id,
           'age' => $this->age,
           'name' => $this->name,
           'weight' => $this->weight,
           'amount_of_legs' => $this->amount_of_legs,
           'created_at' => (string)$this->created_at,
           'updated_at' => (string)$this->updated_at,
       ];
    }
}
