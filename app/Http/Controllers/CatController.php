<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Cat;
use App\Repositories\Repository;
use App\Http\Resources\CatResource;
use App\Http\Requests\CatRequest;

class CatController extends Controller
{
    protected $model;

    public function __construct(Cat $cat)
    {
        $this->model = new Repository($cat);
        CatResource::withoutWrapping();
    }

    /**
     * Display a listing of the resource.
     *
     * @return CatResource
     */
    public function index()
    {
        try {
          $all = $this->model->all();

        } catch (\Exception $err) {
          logger( $err->getMessage());
          return response()->json([ 'status' => false,
                                    'message' => 'Сollection not received',
                                    'model' => null ], 422);
        }
        //return response()->json([ 'status' => true,
        //                  'message' => 'Successful collection',
        //                  'model' => new CatResource($all)], 200);
        return new CatResource($all);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Requests\Http\CatRequest  $request
     * @return CatResource
     */
    public function store(CatRequest $request)
    {
   

     $model = $this->model->create($request->only($this->model->getModel()->fillable));
     //return response()->json([ 'status' => true,
     //                          'message' => 'Succesful create',
     //                          'model' => new CatResource($model)], 200);
	 return new CatResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return CatResource
     */
    public function show($id)
    {
      $model = $this->model->show($id);
     
      if ($model === null)
      {
        return response()->json([ 'status' => false,
                                  'message' => 'Failed search',
                                  'model' => null], 422);

      }
        return new CatResource($model);
      //return response()->json([ 'status' => true,
      //                  'message' => 'Succesful search',
      //                  'model' => new CatResource($model)], 200);
		
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CatRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CatRequest $request, int $id)
    {
   
      $this->model->update($request->only($this->model->getModel()->fillable), $id);

      return response()->json([ 'status' => true,
                                'message' => 'Succesful update',
                                'model' => $this->model->show($id)], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
        $model = $this->model->delete($id);
      } catch (\Exception $err) {
        logger( $err->getMessage());
        return response()->json([ 'status' => false,
                                  'model' => null,
                                  'message' => 'Failed delete'], 404);
      }
      return response()->json([ 'status' => true,
                                'model' => null,
                                'message' => 'Succesful delete'], 410);
    }
}
