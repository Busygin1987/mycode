<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\CatRepository;
use App\Repositories\CatRepositoryInterface;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      //
    }
}
